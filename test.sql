/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50524
Source Host           : 127.0.0.1:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2012-09-07 09:55:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `categories`
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('0', 'Coupe');
INSERT INTO `categories` VALUES ('1', 'SUV');
INSERT INTO `categories` VALUES ('2', 'Sedan');

-- ----------------------------
-- Table structure for `products`
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `description` text CHARACTER SET utf8,
  `tags` varchar(1028) CHARACTER SET utf8 DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('0', 'BMW', '0', null, 'Coupe, Power mirrors, 2 door, V6 Engine, German, 3.0 Liter, 24 Valve, 230 hp', 'BMW328i');
INSERT INTO `products` VALUES ('1', 'BMW', '2', null, 'Sedan, 4 door, V6 Engine, German, 3.0 Liter, 24 Valve, 300 hp', 'BMW535i');
INSERT INTO `products` VALUES ('2', 'BMW', '1', null, 'SUV, 4 door, V6 Engine, German, 3.0 Liter, 24 Valve, 300 hp', 'BMWX5');
INSERT INTO `products` VALUES ('3', 'BMW', '0', null, 'V4 Engine, 2 door, 2 seater, German, 2.0 liter, 16 Valve, 240 hp', 'BMWZ4');
INSERT INTO `products` VALUES ('4', 'Cadillac', '0', null, '2 door, V8 Engine, navigation, American, 6.2 liter, 556 hp', 'CAD-CTS');
INSERT INTO `products` VALUES ('5', 'Cadillac', '2', null, '4 door, 3.6 V6 Engine, American,  304 hp', 'CAD-STS');
INSERT INTO `products` VALUES ('6', 'Lexus', '2', null, '4 door, V6 Engine, Japanese, Low Emission, 306 hp', 'LEX-GS350');
INSERT INTO `products` VALUES ('7', 'Lexus', '1', null, '4 door, V8 Engine, Japanese, 5.7 Liter, 383 hp', 'LEX-LX570');
INSERT INTO `products` VALUES ('8', 'Lexus', '0', null, '2 door,  V6 Engine, Japanese, Navigation, Hardtop, 306 hp', 'LEX-is350C');

-- ----------------------------
-- Table structure for `products_tags`
-- ----------------------------
DROP TABLE IF EXISTS `products_tags`;
CREATE TABLE `products_tags` (
  `id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products_tags
-- ----------------------------
INSERT INTO `products_tags` VALUES ('0', '0', '0');
INSERT INTO `products_tags` VALUES ('1', '0', '1');
INSERT INTO `products_tags` VALUES ('2', '0', '2');
INSERT INTO `products_tags` VALUES ('3', '0', '3');
INSERT INTO `products_tags` VALUES ('4', '0', '4');
INSERT INTO `products_tags` VALUES ('5', '0', '5');
INSERT INTO `products_tags` VALUES ('6', '0', '6');
INSERT INTO `products_tags` VALUES ('7', '0', '7');
INSERT INTO `products_tags` VALUES ('8', '1', '8');
INSERT INTO `products_tags` VALUES ('9', '1', '9');
INSERT INTO `products_tags` VALUES ('10', '1', '3');
INSERT INTO `products_tags` VALUES ('11', '1', '4');
INSERT INTO `products_tags` VALUES ('12', '1', '5');
INSERT INTO `products_tags` VALUES ('13', '1', '6');
INSERT INTO `products_tags` VALUES ('14', '1', '10');
INSERT INTO `products_tags` VALUES ('15', '2', '11');
INSERT INTO `products_tags` VALUES ('16', '2', '9');
INSERT INTO `products_tags` VALUES ('17', '2', '3');
INSERT INTO `products_tags` VALUES ('18', '2', '4');
INSERT INTO `products_tags` VALUES ('19', '2', '5');
INSERT INTO `products_tags` VALUES ('20', '2', '6');
INSERT INTO `products_tags` VALUES ('21', '2', '10');
INSERT INTO `products_tags` VALUES ('22', '3', '12');
INSERT INTO `products_tags` VALUES ('23', '3', '2');
INSERT INTO `products_tags` VALUES ('24', '3', '13');
INSERT INTO `products_tags` VALUES ('25', '3', '4');
INSERT INTO `products_tags` VALUES ('26', '3', '14');
INSERT INTO `products_tags` VALUES ('27', '3', '15');
INSERT INTO `products_tags` VALUES ('28', '3', '16');
INSERT INTO `products_tags` VALUES ('29', '4', '2');
INSERT INTO `products_tags` VALUES ('30', '4', '17');
INSERT INTO `products_tags` VALUES ('31', '4', '18');
INSERT INTO `products_tags` VALUES ('32', '4', '19');
INSERT INTO `products_tags` VALUES ('33', '4', '20');
INSERT INTO `products_tags` VALUES ('34', '4', '21');
INSERT INTO `products_tags` VALUES ('35', '5', '9');
INSERT INTO `products_tags` VALUES ('36', '5', '22');
INSERT INTO `products_tags` VALUES ('37', '5', '3');
INSERT INTO `products_tags` VALUES ('38', '5', '19');
INSERT INTO `products_tags` VALUES ('39', '5', '23');
INSERT INTO `products_tags` VALUES ('40', '6', '9');
INSERT INTO `products_tags` VALUES ('41', '6', '3');
INSERT INTO `products_tags` VALUES ('42', '6', '24');
INSERT INTO `products_tags` VALUES ('43', '6', '25');
INSERT INTO `products_tags` VALUES ('44', '6', '26');
INSERT INTO `products_tags` VALUES ('45', '7', '9');
INSERT INTO `products_tags` VALUES ('46', '7', '17');
INSERT INTO `products_tags` VALUES ('47', '7', '24');
INSERT INTO `products_tags` VALUES ('48', '7', '27');
INSERT INTO `products_tags` VALUES ('49', '7', '28');
INSERT INTO `products_tags` VALUES ('50', '8', '2');
INSERT INTO `products_tags` VALUES ('51', '8', '3');
INSERT INTO `products_tags` VALUES ('52', '8', '24');
INSERT INTO `products_tags` VALUES ('53', '8', '18');
INSERT INTO `products_tags` VALUES ('54', '8', '29');
INSERT INTO `products_tags` VALUES ('55', '8', '26');

-- ----------------------------
-- Table structure for `questions`
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `id` int(11) NOT NULL DEFAULT '0',
  `value` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of questions
-- ----------------------------
INSERT INTO `questions` VALUES ('0', 'Do you prefer American, Japanese or German?');
INSERT INTO `questions` VALUES ('1', 'Do you wish a V4, V6 or V8 Engine?');
INSERT INTO `questions` VALUES ('2', 'How Many liters?');
INSERT INTO `questions` VALUES ('3', 'How many valves?');
INSERT INTO `questions` VALUES ('4', 'How much Horse power?');
INSERT INTO `questions` VALUES ('5', 'How many doors?');
INSERT INTO `questions` VALUES ('6', 'Do you high or low emission?');

-- ----------------------------
-- Table structure for `survey_results`
-- ----------------------------
DROP TABLE IF EXISTS `survey_results`;
CREATE TABLE `survey_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `question_id` varchar(512) DEFAULT NULL,
  `tag_id` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `session_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of survey_results
-- ----------------------------
INSERT INTO `survey_results` VALUES ('1', null, '0', 'American', '0');
INSERT INTO `survey_results` VALUES ('2', null, '0', 'sf', '0');
INSERT INTO `survey_results` VALUES ('3', null, '1', 'sf', '1');
INSERT INTO `survey_results` VALUES ('4', '0000-00-00', '0', 'American', '1');
INSERT INTO `survey_results` VALUES ('5', '0000-00-00', '0', 'American', '1');
INSERT INTO `survey_results` VALUES ('6', '2012-09-06', '0', 'American', '1');
INSERT INTO `survey_results` VALUES ('7', '2012-09-06', '0', 'American', '2');
INSERT INTO `survey_results` VALUES ('8', '2012-09-06', '0', 'American', '3');
INSERT INTO `survey_results` VALUES ('9', '2012-09-06', '0', '19', '4');
INSERT INTO `survey_results` VALUES ('10', '2012-09-06', '1', '17', '5');
INSERT INTO `survey_results` VALUES ('11', '2012-09-07', '0', '19', '6');
INSERT INTO `survey_results` VALUES ('12', '2012-09-07', '0', '19', '7');
INSERT INTO `survey_results` VALUES ('13', '2012-09-07', '0', '19', '8');
INSERT INTO `survey_results` VALUES ('14', '2012-09-07', '0', '24', '9');
INSERT INTO `survey_results` VALUES ('15', '2012-09-07', '1', '17', '9');
INSERT INTO `survey_results` VALUES ('16', '2012-09-07', '5', '2', '9');
INSERT INTO `survey_results` VALUES ('17', '2012-09-07', '0', '4', '10');
INSERT INTO `survey_results` VALUES ('18', '2012-09-07', '0', '4', '11');
INSERT INTO `survey_results` VALUES ('19', '2012-09-07', '0', '19', '12');
INSERT INTO `survey_results` VALUES ('20', '2012-09-07', '0', '19', '13');
INSERT INTO `survey_results` VALUES ('21', '2012-09-07', '0', '24', '14');
INSERT INTO `survey_results` VALUES ('22', '2012-09-07', '0', '19', '15');
INSERT INTO `survey_results` VALUES ('23', '2012-09-07', '1', '12', '15');

-- ----------------------------
-- Table structure for `surveys`
-- ----------------------------
DROP TABLE IF EXISTS `surveys`;
CREATE TABLE `surveys` (
  `id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) DEFAULT NULL,
  `question_id` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of surveys
-- ----------------------------
INSERT INTO `surveys` VALUES ('0', '0', '0');
INSERT INTO `surveys` VALUES ('1', '0', '1');
INSERT INTO `surveys` VALUES ('2', '0', '2');
INSERT INTO `surveys` VALUES ('3', '0', '3');
INSERT INTO `surveys` VALUES ('4', '0', '4');
INSERT INTO `surveys` VALUES ('5', '1', '0');
INSERT INTO `surveys` VALUES ('6', '1', '1');
INSERT INTO `surveys` VALUES ('7', '1', '5');
INSERT INTO `surveys` VALUES ('8', '2', '0');
INSERT INTO `surveys` VALUES ('9', '2', '1');
INSERT INTO `surveys` VALUES ('10', '2', '6');

-- ----------------------------
-- Table structure for `tags`
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tags
-- ----------------------------
INSERT INTO `tags` VALUES ('0', 'Coupe', null);
INSERT INTO `tags` VALUES ('1', 'Power mirrors', null);
INSERT INTO `tags` VALUES ('2', '2 door', '5');
INSERT INTO `tags` VALUES ('3', 'V6 Engine', '1');
INSERT INTO `tags` VALUES ('4', 'German', '0');
INSERT INTO `tags` VALUES ('5', '3.0 Liter', '2');
INSERT INTO `tags` VALUES ('6', '24 Valve', '3');
INSERT INTO `tags` VALUES ('7', '230 hp', '4');
INSERT INTO `tags` VALUES ('8', 'Sedan', null);
INSERT INTO `tags` VALUES ('9', '4 door', '5');
INSERT INTO `tags` VALUES ('10', '300 hp', '4');
INSERT INTO `tags` VALUES ('11', 'SUV', null);
INSERT INTO `tags` VALUES ('12', 'V4 Engine', '1');
INSERT INTO `tags` VALUES ('13', '2 seater', null);
INSERT INTO `tags` VALUES ('14', '2.0 liter', '2');
INSERT INTO `tags` VALUES ('15', '16 Valve', '3');
INSERT INTO `tags` VALUES ('16', '240 hp', '4');
INSERT INTO `tags` VALUES ('17', 'V8 Engine', '1');
INSERT INTO `tags` VALUES ('18', 'navigation', null);
INSERT INTO `tags` VALUES ('19', 'American', '0');
INSERT INTO `tags` VALUES ('20', '6.2 liter', '2');
INSERT INTO `tags` VALUES ('21', '556 hp', '4');
INSERT INTO `tags` VALUES ('22', '3.6 liter', '2');
INSERT INTO `tags` VALUES ('23', '304 hp', '4');
INSERT INTO `tags` VALUES ('24', 'Japanese', '0');
INSERT INTO `tags` VALUES ('25', 'Low Emission', '6');
INSERT INTO `tags` VALUES ('26', '306 hp', '4');
INSERT INTO `tags` VALUES ('27', '5.7 Liter', '2');
INSERT INTO `tags` VALUES ('28', '383 hp', '4');
INSERT INTO `tags` VALUES ('29', 'Hardtop', null);
INSERT INTO `tags` VALUES ('30', '403 hp', '4');
