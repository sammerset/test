#!/usr/bin/env ruby

#need install gems mysql and active_record before use 

require 'rubygems'
require 'mysql'
require 'active_record'

ActiveRecord::Base.establish_connection(
  :adapter  => 'mysql',
  :database => 'test',
  :username => 'sam',
  :password => 'pass',
  :host     => 'localhost'
)

class Category < ActiveRecord::Base
  has_many :products
  has_many :surveys
end

class Product < ActiveRecord::Base
  belongs_to :category
  has_and_belongs_to_many :tags

	scope :by_session_id, lambda { |sess| 
        if !sess.blank?
          {
            :joins => "INNER JOIN products_tags ON products_tags.product_id = products.id INNER JOIN survey_results ON survey_results.tag_id = products_tags.tag_id",
            :conditions => ["session_id = ?", sess]
          }
        end
    }
end

class Survey < ActiveRecord::Base
  belongs_to :category
  belongs_to :question
end

class SurveyResult < ActiveRecord::Base
	belongs_to :tag
end

class Tag < ActiveRecord::Base
  belongs_to :surveys
  belongs_to :question
  has_many :survey_results
  has_and_belongs_to_many :products
  scope :has_surveys, { :joins => "INNER JOIN survey_results ON tags.id = survey_results.tag_id" }
  scope :by_tag_id, lambda { |tag_id| 
    if !tag_id.blank?
      {
        :joins => "INNER JOIN survey_results ON tags.id = survey_results.tag_id",
        :conditions => ["tag_id = ?", tag_id]
      }
    end
  }

end

class Question < ActiveRecord::Base
  has_many :tags
  has_many :surveys
end

class App

  def initialize
    loop do
      puts "Categories"
      Category.all.each_with_index do |c,i|
        printf("%i: %s\n", i+1, c.name)
      end
      puts "Enter your command: "
      in_string = gets.chomp
      if in_string =~ /^select\s*?[a-zA-Z]*$/
        select in_string
      elsif in_string =~ /^survey results$/
        survey_results in_string
      elsif in_string =~ /^products\slist$/
        products_list
      elsif in_string =~ /^delete\sproduct\s*?\S*$/
        delete_product str
      elsif in_string =~ /^(quit|exit)$/
        break
      else
        puts "You may use next commands: \n ==> select <category>\n ==> survey results \n ==> products list \n ==> delete product <id> \n ==> exit(quit)"
      end
      puts "push any button to continie"
      gets
    end
  end

  def select str
    if @category = Category.find_by_name(str.sub(/^select\s*?([a-zA-Z]*)$/,'\1').to_s)
      sr_index = SurveyResult.last.blank? ? 0:SurveyResult.last.session_id+1
      puts "Answer for next questions:"
      @category.surveys.each_with_index{|sur,i|
        printf("%i: %s\n", i+1, sur.question.value)
        puts "Answer may be next:\s"
        sur.question.tags.each_with_index{|t|
          printf("%s\s\|\s", t.name)
        }
        printf "\n"
        answer = gets.chomp
        break if answer.empty?
        @current_tag = sur.question.tags.select{|t| t.name == answer}.first
        if @current_tag.blank?
          puts "Wrong answer! Please try again!"
          redo 
        end
        @sr = SurveyResult.create!(
          :question_id => sur.question.id, 
          :tag_id => @current_tag.id,
          :session_id => sr_index,
          :date => Date.today
        )
      }
      puts "Results products table"
      Product.by_session_id(sr_index).each_with_index{|p,i| printf "%i: %s\n", i+1, p.product_id}
    else
      puts "Category with entered name doesnt exist! Be careful and try again!"
    end
  end

  def survey_results str
    printf "Total surveys count: %i\n", SurveyResult.all.collect{|sr| sr.session_id}.uniq.count
    Tag.has_surveys.uniq.each{|tag|
      printf "%s: %f\%\n", tag.name, (Tag.by_tag_id(tag.id).count/Tag.has_surveys.count.to_f)*100
    }
  end

  def products_list
    Product.all.each_with_index{|p,i|
      printf "%i: %s\n", i+1, p.product_id
    }
  end

  def delete_product str
    if @product = Product.find_by_product_id(str.sub(/^delete\sproduct\s*?(\S*)$/,'\1').to_s)
      name=[]
      name<<@product.product_id
      puts name.first
      if @product.destroy
        printf "Product with id %s destroyed!\n", name.first
      else
        puts "Undefined error!"
      end
    else
      puts "This product doesnt exist!"
    end
  end

end

App.new