For start to use this programm, need next steps:
 - unpack test.sql and load database into Mysql-server;
 - install needed gems (gem install rubygems, gem install mysql, gem install active_record);
 - go to working directory with cd command in active console (ex. $ cd /home/user/file);
 - edit file qp.rb (change ActiveRecord::Base config to own credentials, such as user, password, host);
 - push next command - $ ruby ./qp.rb;
 - use one of next commands - select <category>, survey results, products list, delete product <id>, exit (or quit).

 Thats all!